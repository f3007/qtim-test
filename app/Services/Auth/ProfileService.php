<?php
namespace App\Services\Auth;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ChangeProfileRequest;
use App\Http\Traits\Hashable;
use App\Http\Traits\Imagable;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Role;

class ProfileService extends TokenIssueService {

    use Hashable, Imagable;

    public function get(Request $request) {
        return User::find(auth('api')->user()->id);
    }

    public function logout(Request $request) {
        auth('api')->logout();
        return response()->json(['message' => 'Вы успешно вышли из аккаунта!']);
    }

}
