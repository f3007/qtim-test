<?php

namespace App\Services\Auth;

use App\Http\Requests\EnterCodeAndPasswordRequest;
use App\Http\Requests\SendResetCodeRequest;
use App\Http\Traits\Cachable;
use App\Http\Traits\Findable;
use App\Http\Traits\Hashable;
use function response;

class PasswordResetService extends SendCodeService
{
    use Cachable, Findable, Hashable;

    public function sendCode(SendResetCodeRequest $request) {
        $data = $request->validated();
        $key = $this->putCachedData($data);
        $code = rand(111111, 777777);
        if (!empty($data['email'])) {
            $this->sendEmail($data['email'], $code);
            return response()->json(['message' => 'Код для восстановления пароля отправлен на вашу почту!', 'key' => $key]);
        } else {
            $this->sendSms($data['phone_number'], $code);
            return response()->json(['message' => 'Код для восстановления пароля отправлен на ваш номер телефона!', 'key' => $key]);
        }
    }

    public function enterCodeAndPassword(EnterCodeAndPasswordRequest $request) {
        $data = $request->validated();
        $credentials = $this->getCachedData($data['key']);
        $this->deleteCachedData($data['key']);
        $user = !empty($credentials['email']) ? $this->byEmail($credentials['email']) : $this->byPhone($credentials['phone_number']);
        $user->password = $this->hashPassword($data['new_password']);
        $user->save();
        return response()->json(['message' => 'Ваш пароль обновлен!']);
    }
}
