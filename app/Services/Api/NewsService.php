<?php
namespace App\Services\Api;

use App\Http\Requests\NewsStoreRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Http\Traits\Imagable;
use App\Models\News;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsService {

    use Imagable;

    public function index() {
        return News::all();
    }

    public function show($id) {
        return News::find($id);
    }

    public function store(NewsStoreRequest $storeRequest) {
        $data = $storeRequest->validated();
        $file = $this->fromBase64($data['img']);
        Storage::disk(config('voyager.storage.disk'))->put('',$file);
        News::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'img' => $file->hashName()
        ]);
        return response()->json(['message' => 'Новость создана!']);
    }

    public function update(NewsUpdateRequest $updateRequest, $id) {
        $user = User::find(auth('api')->user()->id);
        $data = $updateRequest->validated();
        $news = News::find($id);
        $file = $this->fromBase64($data['img']);
        Storage::disk(config('voyager.storage.disk'))->delete('',$news->img);
        Storage::disk(config('voyager.storage.disk'))->put('',$file);
        $news->update([
            'title' => $data['title'],
            'description' => $data['description'],
            'img' => $file->hashName()
        ]);
        return response()->json(['message' => 'Новость изменена!']);
    }

    public function destroy(Request $request, $id) {
        $user = User::find(auth('api')->user()->id);
        $news = $this->show($request, $id);
        $news->delete();
        return response()->json(['message' => 'Новость удалена!']);
    }
}
