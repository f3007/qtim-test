<?php

namespace App\Http\Traits;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait Imagable {

    public function fromBase64(string $base64File): UploadedFile
    {
        // Get file data base64 string
        $fileData = base64_decode(Arr::last(explode(',', $base64File)));

        // Create temp file and get its absolute path
        $tempFile = tmpfile();
        $tempFilePath = stream_get_meta_data($tempFile)['uri'];

        // Save file data in file
        file_put_contents($tempFilePath, $fileData);

        $tempFileObject = new File($tempFilePath);
        $file = new UploadedFile(
            $tempFileObject->getPathname(),
            $tempFileObject->getFilename(),
            $tempFileObject->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );

        // Close this file after response is sent.
        // Closing the file will cause to remove it from temp director!
        app()->terminating(function () use ($tempFile) {
            fclose($tempFile);
        });

        // return UploadedFile object
        return $file;
    }

    public function getStorageLinkAttribute($img_str) {
        return Storage::disk(config('voyager.storage.disk'))->url($img_str);
    }

    public function getPhotosArrayAttribute($arr_str) {
        $arr = json_decode($arr_str);
        $length = count((array)$arr);
        for ($i = 0; $i < $length; $i++) {
            $arr[$i] = $this->getStorageLinkAttribute($arr[$i]);
        }
        return $arr;
    }



}
